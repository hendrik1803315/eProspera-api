# eProspera API Documentation

Welcome to the official documentation for the eProspera API. This document provides comprehensive information on how to integrate and use the eProspera API.

## Getting started

### Prerequisites

* Node https://nodejs.org/en/
* NPM
* Docker https://www.docker.com/
 * Install Docker Desktop for MAC: https://docs.docker.com/docker-for-mac/install/
 * Install Docker Desktop for Windows: https://docs.docker.com/docker-for-windows/install/
* Docker Compose
* NestJS CLI https://nestjs.com/

### Setup

To get started, clone the repository to your local computer. Use the following command to run in your terminal.

#### Clone The Application

```bash
# clone the application
$ git clone https://gitlab.com/hendrik1803315/eProspera-api.git
```

#### Install The Dependencies

Next, install the packages that are required for this project.

```bash
// Install the required npm modules
$ npm install
```

#### Create The Environment Variables

Create a .env file in the root folder with the following variables: 
* .env : General Environment File

```
DB_URI=mongodb://localhost:27018/eprospera-api
```

#### Start MongoDB Database

In order to use mongodb, we will be using Docker Compose, In this project root directory, execute the following command:

```
# build images, create and run containers in background
docker-compose up -d
```

#### Running The App
```bash
# Start application
$ npm run start

# Run in development mode
$ npm run start:dev

# Run in production mode
$ npm run start:prod
```
#### Test

In this project, I chose Jest as my test framework. The following are the test commands:

```bash
# Run all unit tests
$ npm run test

# Run users unit tests
$ npm test users.controller

# Run residents unit tests
$ npm test residents.controller

# Run IndustryChangeApplication tests
$ npm test industry-change-application.controller
```

#### Runnin The Build

All the different build steps are orchestrated via npm scripts. Npm scripts basically allow us to call (and chain) terminal commands via npm. This is nice because most JavaScript tools have easy to use command line utilities allowing us to not need grunt or gulp to manage our builds. If you open package.json, you will see a scripts section with all the different scripts you can call. To call a script, simply run npm run <script-name> from the command line. You'll notice that npm scripts can call each other which makes it easy to compose complex builds out of simple individual build scripts. Below is a list of all the scripts this template has available:

| Npm Script | Description      |
|----------|----------------------|
| build    | Full build. Runs ALL build tasks   |
| format    | Runs the file formatter   |
| lint    | Runs ESLint on project files   |
| start:debug   | Performs a full build and then serves the app in watch mode   |
| start    | Does the same as 'npm run serve'. Can be invoked with 'npm start'   |
| test   | Runs tests using Jest test runner   |
| test:watch   | Runs tests in watch mode   |

### Project Structure

In a TypeScript project, it's best to have separate source and distributable files. TypeScript (.ts) files live in your src folder and after compilation are output as JavaScript (.js) in the dist folder. The test and views folders remain top level as expected.

Please find below a detailed description of the app's folder structures:   
**Note! Make sure you have already built the app using 'npm run build'**

| Name     | Description        |
|--------------|------------------|
| dist      | Contains the distributable (or output) from the TypeScript build. This is the code you ship          |
| node_modules      | Contains all the npm dependencies          |
| src      | Contains your source code that will be compiled to the dist dir          |
| src/industry-change-application      | Contains all files that are associated with the industry-change-application object         |
| src/industry-change-application/__mocks__      | Contains mock service methods, for testing purposes.          |
| src/industry-change-application/dto      | Contains Industry Change Application dto's, for clarifying request payloads.          |
| src/industry-change-application/schemas      | Contains industry-change-application schema files         |
| src/industry-change-application/test      | Contains a test file that includes all unit tests related to Industry Change Application          |
| src/industry-change-application/industry-change-application.controller.ts     | Controller file for Industry Change Application Requests          |
| src/industry-change-application/industry-change-application.module.ts      | Module file for Industry Change Application         |
| src/industry-change-application/industry-change-application.service.ts      | Service file that includes all the logic for Industry Change Applications          |
| src/resident      | Contains all files that are associated with the resident object         |
| src/resident/__mocks__      | Contains mock service methods, for testing purposes.          |
| src/resident/dto      | Contains Resident dto's, for clarifying request payloads.          |
| src/resident/schemas      | Contains resident schema files         |
| src/resident/test      | Contains a test file that includes all unit tests related to Resident          |
| src/resident/add-resident.ts      | This is a script file for adding Residents           |
| src/resident/resident.controller.ts     | Controller file for Resident Requests          |
| src/resident/resident.module.ts      | Module file for Resident         |
| src/resident/resident.service.ts      | Service file that includes all the logic for Resident          |
| src/users      | Contains all files that are associated with the User object and Authentication         |
| src/users/__mocks__      | Contains mock service methods, for testing purposes.          |
| src/users/dto      | Contains Users dto's, for clarifying request payloads.          |
| src/users/middlewares      | Contains middleware.          |
| src/users/schemas      | Contains Users schema files         |
| src/users/test      | Contains a test file that includes all unit tests related to Users          |
| src/users/types      | Contains Users response type          |
| src/users/users.constants.ts     | Contains jwt token secret          |
| src/users/users.controller.ts     | Controller file for User Requests          |
| src/users/users.guard.ts     | UserGuard file that handles User and token authentication          |
| src/users/users.module.ts      | Module file for Users         |
| src/users/users.service.ts      | Service file that includes all the logic for Users          |


## Documentation

### 1. Start Application

Start the application with the following command:
**Note! Make sure that the mongodb is running using command 'docker-compose up -d'**
```bash
# Start Application
$ npm run start
```
The Application starts on path: http://localhost:3000. All of the API calls are made to this local url. You will also find a swaggerUI API documentation on: http://localhost:3000/api .

### 2. Run The add-residents Script

Run the following command to add Demo residents:
```bash
# Add Demo Residents
$ npx ts-node ./src/resident/add-residents.ts
```
This script adds 3 Residents to the database.

### 3. Authentication

This step is mandatory, because to access the rest of the API you will need a JWToken. All the Users API methods Create user, Login user and get user return a JWToken.
```
POST /users
{
  "email": "samle@example.com",
  "username": "sampleUser",
  "password": "samplePass123"
}
```
This returns the created user object with the JWToken.
Later, if you need to renew your token or create a new one you can use the login method: 
```
POST /users/login
{
  "email": "samle@example.com",
  "password": "samplePass123"
}
```
This returns the logged in user object with the JWToken.

### 4. Using the API

#### Industry Change Applications

Now that all of the setup is completed we can use this API to the fullest. Lets start with a demonstration of the GET /industry-change-application method:
```
GET /industry-change-application
# The parameters are used as filters for the API query. If parameters are left empty, then it returns all CURRENT Applications.
Parameters:
# statuses filter allows user to choose which applications do they want to query. Possible values: IN_REVIEW,APPROVED,REJECTED. Please make sure that the parameter is coma separated.
statuses: IN_REVIEW
# residentSub filter allows user to choose the resident of which applications they want to see. Insert resident subId
```
**Don't forget to add the Authorization header to the request: Authorization: Token {your_JWToken}**   
Deeper explenation of this request is in /eProspera BEX/GET resident-registerindustry-change-applications.pdf file.

The next API query we will take a look at is for adding Industry Change Applications. This query is for adding applications to the database:   
**Make sure to use look up relevant residendSub values**  
```
POST /industry-change-application
{
"residentSub": "8e67821f-88af-4425-8eda-da45b9d44f9e",
  "requested": {
    "willWorkInPhysicalJurisdiction": true,
    "industry": "CONSTRUCTION",
    "regulatoryElection": "AUSTRALIA",
    "regulatoryElectionSub": ""
  }
}
```
Deeper explenation of this request is in /eProspera BEX/POST resident-registerindustry-change-applications.pdf file.

Now that we have added some Industry Change Applications we can start modifing them. For that we have three different requests: 
```
# This method does not delete Application from database, but it deletes it logically(ObjectStatus).
PUT /industry-change-application/delete/{id}

# This method approves Industry Change Applications, that are in 'IN_REVIEW' status.
PUT /industry-change-application/approve/{id}

# This method rejects Industry Change Applications, that are in 'IN_REVIEW' status.
PUT /industry-change-application/reject/{id}

```

#### Residents
I also added a API to residents with CRUD possibilities. With this you can query residents, create residents, find residents by subId, update residents by subId and also delete residents from database using subId.

```
# This method collects all residents from database and returns them.
GET /residents

# With this method you can add new residents to the database. Make sure to include payload:
POST /residents/new
{
  "firstName": "Thomas",
  "lastName": "Anderson",
  "fullName": "Thomas Anderson",
  "permitNumber": 5253453,
  "permitNumberQrCode": "67435321",
  "dateOfBirth": "1990-01-01",
  "countryOfBirth": "USA",
  "email": "thomas.anderson@sample.com",
  "citizenship": "USA",
  "gender": "Male",
  "address": {
    "country": "USA",
    "city": "New York",
    "streetAddress": "313 Morris Ave",
    "zipCode": "07103",
    "isVerifiedAddress": true
  },
  "phoneNumber": "2125724022",
  "typeOfRegistration": "LIMITED_E_RESIDENCY",
  "typeOfRegistrationSub": "INTERNATIONAL",
  "industry": "WASTE_MANAGEMENT",
  "willWorkInPhysicalJurisdiction": false,
  "regulatoryElection": "USA",
  "regulatoryElectionSub": "",
  "firstRegistrationDate": "2024-03-03",
  "nextSubscriptionPaymentDate": "2024-06-06",
  "profilePicture": "https://www.w3schools.com/html/img_girl.jpg",
  "status": "ACTIVE",
  "residencyEndDate": "2025-06-01"
}

# This method is for finding specific residents using their subId.
GET /residents/{subId}

# This is a update method for updating residents. None of the fields in this payload is mandatory:
PUT /residents/{subId}
{
  "firstName": "Thomas",
  "lastName": "Anderson",
  "fullName": "Thomas Anderson",
  "permitNumber": 5253453,
  "permitNumberQrCode": "67435321",
  "dateOfBirth": "1990-01-01",
  "countryOfBirth": "USA",
  "email": "thomas.anderson@sample.com",
  "citizenship": "USA",
  "gender": "Male",
  "address": {
    "country": "USA",
    "city": "New York",
    "streetAddress": "313 Morris Ave",
    "zipCode": "07103",
    "isVerifiedAddress": true
  },
  "phoneNumber": "2125724022",
  "typeOfRegistration": "LIMITED_E_RESIDENCY",
  "typeOfRegistrationSub": "INTERNATIONAL",
  "industry": "WASTE_MANAGEMENT",
  "willWorkInPhysicalJurisdiction": false,
  "regulatoryElection": "USA",
  "regulatoryElectionSub": "",
  "firstRegistrationDate": "2024-03-03",
  "nextSubscriptionPaymentDate": "2024-06-06",
  "profilePicture": "https://www.w3schools.com/html/img_girl.jpg",
  "status": "ACTIVE",
  "residencyEndDate": "2025-06-01"
}

# And the last method is for deleting residents, using their subId.
DELETE /residents/{subId}
```

### Request and Response Cycle

All of the requests follow a similar cycle Request -> Middleware -> UserGuard -> Validation Pipes -> Controller -> Service -> Response. Only that the User API methods don't pass through the UserGurad, because without token you cannot authorize your requests. 

### Unit Tests

I chose to create my jest unit tests for the controllers themselves. Because while testing controllers I could test multiple things in the controller level, service level and also if the correct value is actually returned.

## Conclusion

The process of building this API was very fun and informative. Nestjs documentation and tutorials make it really easy to use.   
If you have any questions or god forbid bugs, please feel free to contact me:   
hendrik.metsallik@gmail.com