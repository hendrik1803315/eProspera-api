import { industryChangeApplicationStub } from '../test/stubs/industry-change-application.stub';

export const IndustryChangeApplicationServiceMock = jest.fn().mockReturnValue({
  find: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
  create: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
  findById: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
  delete: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
  approve: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
  reject: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
});
