import {
  IsNotEmpty,
  IsEnum,
  IsObject,
  ValidateNested,
  Matches,
  IsString,
  IsDefined,
} from 'class-validator';
import { Type } from 'class-transformer';
import {
  Industry,
  RegulatoryElection,
} from '../schemas/industry-change-application.schema';
import { ApiProperty } from '@nestjs/swagger';

class Requested {
  @IsDefined()
  @ApiProperty({
    description: 'Will work in physical juristiction',
    example: false,
  })
  willWorkInPhysicalJurisdiction: boolean;

  @IsEnum(Industry)
  @IsDefined()
  @ApiProperty({
    description: 'Industry',
    example: 'ENERGY',
  })
  industry?: Industry;

  @IsDefined()
  @IsEnum(RegulatoryElection)
  @ApiProperty({
    description: 'Regulatory election',
    example: 'USA',
  })
  regulatoryElection?: RegulatoryElection;

  @IsString()
  @IsDefined()
  @ApiProperty({
    description: 'Regulatory election sub',
    example: '',
  })
  regulatoryElectionSub?: string;
}

export class CreateIndustryChangeApplicationDto {
  @IsNotEmpty()
  @IsDefined()
  @Matches(
    /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/,
    { message: 'Invalid Amazon Cognito identity pool ID format' },
  )
  @IsString()
  @ApiProperty({
    description: 'Resident sub',
    example: '8e67821f-88af-4425-8eda-da45b9d44f9e',
  })
  residentSub: string;

  @IsDefined()
  @IsNotEmpty({ message: 'Requested object must be provided' })
  @IsObject()
  @ValidateNested()
  @Type(() => Requested)
  @ApiProperty({
    description: 'Requested',
    example: {
      willWorkInPhysicalJurisdiction: true,
      industry: 'CONSTRUCTION',
      regulatoryElection: 'AUSTRALIA',
      regulatoryElectionSub: '',
    },
  })
  requested: Requested;
}
