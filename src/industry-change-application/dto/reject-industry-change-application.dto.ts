import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';

export class RejectIndustryChangeApplicationDto {
  @IsString()
  @IsDefined()
  @ApiProperty({
    description: 'Rejection reason',
    example: 'Not available to change to that industry.',
  })
  rejectionReason: string;
}
