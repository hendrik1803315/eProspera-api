import {
  Body,
  Controller,
  Get,
  Post,
  Headers,
  Param,
  Query,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  IcaResponse,
  IndustryChangeApplicationService,
} from './industry-change-application.service';
import { CreateIndustryChangeApplicationDto } from './dto/create-industry-change-application.dto';
import { UserGuard } from '../users/users.guard';
import { RejectIndustryChangeApplicationDto } from './dto/reject-industry-change-application.dto';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('IndustryChangeApplication')
@Controller('industry-change-application')
export class IndustryChangeApplicationController {
  constructor(private service: IndustryChangeApplicationService) {}

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Found industry change application.',
  })
  @ApiBadRequestResponse({
    description: 'Could not find a industry change application.',
  })
  @Get()
  async getIndustryChangeApplications(
    @Query('statuses') statusParam: string,
    @Query('residentSub') residentSub: string,
  ): Promise<IcaResponse> {
    return this.service.find(statusParam, residentSub);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Created a industry change application.',
  })
  @ApiBadRequestResponse({
    description: 'Could not create a industry change application.',
  })
  @Post()
  async createIndustryChangeApplication(
    @Body() createDto: CreateIndustryChangeApplicationDto,
    @Headers('authorization') token: string,
  ): Promise<IcaResponse> {
    return await this.service.create(createDto, token);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Found a industry change application by id.',
  })
  @ApiBadRequestResponse({
    description: 'Could not find a industry change application with that id.',
  })
  @Get(':id')
  async getIndustryChangeApplicationsById(
    @Param('id')
    id: string,
  ): Promise<IcaResponse> {
    return this.service.findById(id);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Deleted a industry change application.',
  })
  @ApiBadRequestResponse({
    description: 'Could not delete this industry change application.',
  })
  @Put('delete/:id')
  async deleteIndustryChangeApplication(
    @Param('id')
    id: string,
    @Headers('authorization') token: string,
  ): Promise<IcaResponse> {
    return this.service.delete(id, token);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Approved this industry change application.',
  })
  @ApiBadRequestResponse({
    description: 'Could not approve this industry change application.',
  })
  @Put('approve/:id')
  async approveIndustryChangeApplication(
    @Param('id')
    id: string,
    @Headers('authorization') token: string,
  ): Promise<IcaResponse> {
    return this.service.approve(id, token);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Rejected this industry change application.',
  })
  @ApiBadRequestResponse({
    description: 'Could not reject this industry change application.',
  })
  @Put('reject/:id')
  async rejectIndustryChangeApplication(
    @Param('id')
    id: string,
    @Headers('authorization') token: string,
    @Body() rejectDto: RejectIndustryChangeApplicationDto,
  ): Promise<IcaResponse> {
    return this.service.reject(id, token, rejectDto);
  }
}
