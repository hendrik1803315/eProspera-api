import { Module } from '@nestjs/common';
import { IndustryChangeApplicationController } from './industry-change-application.controller';
import { IndustryChangeApplicationService } from './industry-change-application.service';
import { MongooseModule } from '@nestjs/mongoose';
import { IndustryChangeApplicationSchema } from './schemas/industry-change-application.schema';
import { ResidentService } from 'src/resident/resident.service';
import { ResidentSchema } from 'src/resident/schemas/resident.schema';
import { UserEntitySchema } from 'src/users/schemas/users.schema';
import { UsersService } from 'src/users/users.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'IndustryChangeApplication',
        schema: IndustryChangeApplicationSchema,
      },
      { name: 'Resident', schema: ResidentSchema },
      { name: 'Users', schema: UserEntitySchema },
    ]),
  ],
  controllers: [IndustryChangeApplicationController],
  providers: [IndustryChangeApplicationService, ResidentService, UsersService],
})
export class IndustryChangeApplicationModule {}
