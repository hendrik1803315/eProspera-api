import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  ApplicationStatus,
  IndustryChangeApplication,
  ObjectStatus,
} from './schemas/industry-change-application.schema';
import * as mongoose from 'mongoose';
import { CreateIndustryChangeApplicationDto } from './dto/create-industry-change-application.dto';
import { ResidentService } from '../resident/resident.service';
import {
  RegistrationType,
  Status as ResidentStatus,
} from '../resident/schemas/resident.schema';
import { UsersService } from '../users/users.service';
import { RejectIndustryChangeApplicationDto } from './dto/reject-industry-change-application.dto';

export interface IcaResponse {
  statusCode: number;
  message: string;
  data?: IndustryChangeApplication;
  dataArray?: IndustryChangeApplication[];
  error?: any;
}

@Injectable()
export class IndustryChangeApplicationService {
  constructor(
    @InjectModel(IndustryChangeApplication.name)
    private industryChangeApplicationModel: mongoose.Model<IndustryChangeApplication>,
    private readonly residentService: ResidentService,
    private readonly usersService: UsersService,
  ) {}

  async find(statusParam: string, residentSub: string): Promise<IcaResponse> {
    let statuses = [];
    if (typeof statuses === 'string') {
      statuses = statusParam.split(',');
    }

    if (Array.isArray(statuses)) {
      statuses = statuses.map((status) => status.toUpperCase());
    }

    const filter: any = {};
    if (statuses && statuses.length > 0) {
      filter.status = { $in: statuses };
    }

    if (residentSub) {
      filter.residentSub = residentSub;
    }

    try {
      filter.objectStatus = ObjectStatus.CURRENT;

      const res = (await this.industryChangeApplicationModel.find(
        filter,
      )) as IndustryChangeApplication[];

      if (res.length === 0) {
        throw new BadRequestException('No industry change applications found.');
      }

      return {
        statusCode: HttpStatus.CREATED,
        message: 'IndustryChangeApplication found successfully',
        dataArray: res,
      };
    } catch (error) {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal server error',
        error: error.message,
      };
    }
  }

  async create(
    icaDto: CreateIndustryChangeApplicationDto,
    token: string,
  ): Promise<IcaResponse> {
    token = token.split(' ')[1];
    const user = await this.usersService.getCurrentUser(token);

    if (!user) {
      throw new HttpException(
        'Not authorized',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    try {
      const resident = await this.residentService.findById(icaDto.residentSub);
      let status = ApplicationStatus.IN_REVIEW;
      let decidedAt: Date | null = null;
      let decidedBy: string | null = null;

      if (
        resident.typeOfRegistration !== RegistrationType.E_RESIDENCY &&
        resident.typeOfRegistration !== RegistrationType.RESIDENCY
      ) {
        throw new BadRequestException(
          'Resident has invalid type of registration.',
        );
      }

      if (resident.status !== ResidentStatus.ACTIVE) {
        throw new BadRequestException('Resident is not active.');
      }

      if (
        icaDto.requested.willWorkInPhysicalJurisdiction ===
          resident.willWorkInPhysicalJurisdiction &&
        icaDto.requested.industry === resident.industry &&
        icaDto.requested.regulatoryElection === resident.regulatoryElection &&
        icaDto.requested.regulatoryElectionSub ===
          resident.regulatoryElectionSub
      ) {
        throw new BadRequestException(
          "Requested industry information matches with resident's current industry information.",
        );
      }

      if (!icaDto.requested.willWorkInPhysicalJurisdiction) {
        status = ApplicationStatus.APPROVED;
      } else {
        status = ApplicationStatus.IN_REVIEW;
      }

      switch (status) {
        case 'IN_REVIEW':
          decidedAt = null;
          decidedBy = null;
          break;
        case 'APPROVED':
          decidedAt = new Date();
          decidedBy = user._id;
          break;
        default:
          decidedAt = null;
          decidedBy = null;
          break;
      }

      const industryChangeApplication = new this.industryChangeApplicationModel(
        {
          residentSub: icaDto.residentSub,
          current: {
            willWorkInPhysicalJurisdiction:
              resident.willWorkInPhysicalJurisdiction,
            industry: resident.industry,
            regulatoryElection: resident.regulatoryElection,
            regulatoryElectionSub: resident.regulatoryElectionSub,
          },
          requested: {
            willWorkInPhysicalJurisdiction:
              icaDto.requested.willWorkInPhysicalJurisdiction,
            industry: icaDto.requested.industry,
            regulatoryElection: icaDto.requested.regulatoryElection,
            regulatoryElectionSub: icaDto.requested.regulatoryElectionSub,
          },
          status: status,
          submittedAt: new Date(),
          decision: {
            decidedAt: decidedAt,
            decidedBy: decidedBy,
            rejectionReason: null,
          },
          createdAt: new Date(),
          createdBy: user._id,
          updatedAt: new Date(),
          updatedBy: user._id,
          objectStatus: ObjectStatus.CURRENT,
        },
      );

      if (industryChangeApplication.status === ApplicationStatus.APPROVED) {
        resident.willWorkInPhysicalJurisdiction =
          icaDto.requested.willWorkInPhysicalJurisdiction;
        resident.industry = icaDto.requested.industry;
        resident.regulatoryElection = icaDto.requested.regulatoryElection;
        resident.regulatoryElectionSub = icaDto.requested.regulatoryElectionSub;

        await this.residentService.updateById(resident.subID, resident);
      }

      await industryChangeApplication.save();

      return {
        statusCode: HttpStatus.CREATED,
        message: 'IndustryChangeApplication created successfully',
        data: industryChangeApplication,
      };
    } catch (error) {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal server error',
        error: error.message,
      };
    }
  }

  async findById(id: string): Promise<IcaResponse> {
    try {
      const ica = await this.industryChangeApplicationModel.findOne({
        _id: id,
      });
      if (!ica) {
        throw new NotFoundException('IndustryChangeApplication not found');
      }
      if (ica.objectStatus !== ObjectStatus.CURRENT) {
        throw new BadRequestException(
          'IndustryChangeApplication has been deleted',
        );
      }
      return {
        statusCode: HttpStatus.FOUND,
        message: 'IndustryChangeApplication found',
        data: ica,
      };
    } catch (error) {
      if (error.name === 'CastError' && error.kind === 'ObjectId') {
        throw new BadRequestException('Invalid IndustryChangeApplication ID');
      } else {
        return {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'Internal server error',
          error: error.message,
        };
      }
    }
  }

  async delete(id: string, token: string): Promise<IcaResponse> {
    token = token.split(' ')[1];
    const user = await this.usersService.getCurrentUser(token);

    if (!user) {
      throw new HttpException(
        'Not authorized',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    const ica = await this.industryChangeApplicationModel.findOne({ _id: id });

    if (!ica) {
      throw new NotFoundException('IndustryChangeApplication not found');
    }

    if (ica.status !== ApplicationStatus.IN_REVIEW) {
      throw new BadRequestException(
        'Cannot delete IndustryChangeApplication, if status is not IN_REVIEW',
      );
    }

    try {
      ica.objectStatus = ObjectStatus.DELETED;
      ica.updatedAt = new Date();
      ica.updatedBy = user._id;

      const industryChangeApplication =
        await this.industryChangeApplicationModel.findByIdAndUpdate(id, ica);

      return {
        statusCode: HttpStatus.ACCEPTED,
        message: 'IndustryChangeApplication deleted successfully',
        data: industryChangeApplication,
      };
    } catch (error) {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal server error',
        error: error.message,
      };
    }
  }

  async approve(id: string, token: string): Promise<IcaResponse> {
    token = token.split(' ')[1];
    const user = await this.usersService.getCurrentUser(token);

    if (!user) {
      throw new HttpException(
        'Not authorized',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    const ica = await this.industryChangeApplicationModel.findOne({ _id: id });

    if (!ica) {
      throw new NotFoundException('IndustryChangeApplication not found');
    }

    if (ica.objectStatus === ObjectStatus.DELETED) {
      throw new BadRequestException(
        'IndustryChangeApplication has been deleted.',
      );
    }

    if (ica.status === ApplicationStatus.APPROVED) {
      throw new BadRequestException(
        'Cannot approve IndustryChangeApplication, because because application is already approved.',
      );
    }

    try {
      const resident = await this.residentService.findById(ica.residentSub);

      ica.status = ApplicationStatus.APPROVED;
      ica.updatedAt = new Date();
      ica.updatedBy = user._id;
      ica.decision.decidedBy = user._id;
      ica.decision.decidedAt = new Date();
      ica.current.willWorkInPhysicalJurisdiction =
        ica.requested.willWorkInPhysicalJurisdiction;
      ica.current.industry = ica.requested.industry;
      ica.current.regulatoryElection = ica.requested.regulatoryElection;
      ica.current.regulatoryElectionSub = ica.requested.regulatoryElectionSub;
      resident.willWorkInPhysicalJurisdiction =
        ica.requested.willWorkInPhysicalJurisdiction;
      resident.industry = ica.requested.industry;
      resident.regulatoryElection = ica.requested.regulatoryElection;
      resident.regulatoryElectionSub = ica.requested.regulatoryElectionSub;

      const industryChangeApplication =
        await this.industryChangeApplicationModel.findByIdAndUpdate(id, ica);
      await this.residentService.updateById(resident.subID, resident);

      return {
        statusCode: HttpStatus.ACCEPTED,
        message: 'IndustryChangeApplication approved successfully',
        data: industryChangeApplication,
      };
    } catch (error) {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal server error',
        error: error.message,
      };
    }
  }

  async reject(
    id: string,
    token: string,
    rejectDto: RejectIndustryChangeApplicationDto,
  ): Promise<IcaResponse> {
    token = token.split(' ')[1];
    const user = await this.usersService.getCurrentUser(token);

    if (!user) {
      throw new HttpException(
        'Not authorized',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    const ica = await this.industryChangeApplicationModel.findOne({ _id: id });

    if (!ica) {
      throw new NotFoundException('IndustryChangeApplication not found');
    }

    if (ica.objectStatus === ObjectStatus.DELETED) {
      throw new BadRequestException(
        'IndustryChangeApplication has been deleted.',
      );
    }

    if (ica.status !== ApplicationStatus.IN_REVIEW) {
      throw new BadRequestException(
        'Cannot reject IndustryChangeApplication, if status is not IN_REVIEW',
      );
    }

    try {
      ica.status = ApplicationStatus.REJECTED;
      ica.updatedAt = new Date();
      ica.updatedBy = user._id;
      ica.decision.decidedAt = new Date();
      ica.decision.decidedBy = user._id;
      ica.decision.rejectionReason = rejectDto.rejectionReason;

      const industryChangeApplication =
        await this.industryChangeApplicationModel.findByIdAndUpdate(id, ica);

      return {
        statusCode: HttpStatus.ACCEPTED,
        message: 'IndustryChangeApplication rejected successfully',
        data: industryChangeApplication,
      };
    } catch (error) {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal server error',
        error: error.message,
      };
    }
  }
}
