import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Decision {
  @Prop()
  decidedAt?: Date;

  @Prop()
  decidedBy?: string;

  @Prop()
  rejectionReason?: string;
}
