import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Current } from './current.schema';
import { Requested } from './requested.schema';
import { Decision } from './decision.schema';

export enum Industry {
  AGRICULTURAL = 'AGRICULTURAL',
  CONSTRUCTION = 'CONSTRUCTION',
  ENERGY = 'ENERGY',
  FINANCE_AND_INSURANCE = 'FINANCE_AND_INSURANCE',
  FOOD = 'FOOD',
  HEALTH = 'HEALTH',
  MANUFACTURING = 'MANUFACTURING',
  MINING_AND_SUBSURFACE = 'MINING_AND_SUBSURFACE',
  PRIVATE_SECURITY = 'PRIVATE_SECURITY',
  WASTE_MANAGEMENT = 'WASTE_MANAGEMENT',
}

export enum RegulatoryElection {
  AUSTRALIA = 'AUSTRALIA',
  AUSTRIA = 'AUSTRIA',
  BELGIUM = 'BELGIUM',
  CANADA = 'CANADA',
  CHILE = 'CHILE',
  ROATAN_COMMON_LAW_CODE = 'ROATAN_COMMON_LAW_CODE',
  DENMARK = 'DENMARK',
  DUBAI = 'DUBAI',
  ESTONIA = 'ESTONIA',
  FINLAND = 'FINLAND',
  FRANCE = 'FRANCE',
  GERMANY = 'GERMANY',
  HONDURAS = 'HONDURAS',
  HONG_KONG = 'HONG_KONG',
  ICELAND = 'ICELAND',
  IRELAND = 'IRELAND',
  ISRAEL = 'ISRAEL',
  ITALY = 'ITALY',
  JAPAN = 'JAPAN',
  LUXEMBOURG = 'LUXEMBOURG',
  MEXICO = 'MEXICO',
  NETHERLANDS = 'NETHERLANDS',
  NEW_ZEALAND = 'NEW_ZEALAND',
  null = 'null',
  NORWAY = 'NORWAY',
  PETITION_FOR_TAILORED_REGULATION_GRANTED = 'PETITION_FOR_TAILORED_REGULATION_GRANTED',
  PETITION_FOR_TAILORED_REGULATION_PENDING = 'PETITION_FOR_TAILORED_REGULATION_PENDING',
  POLAND = 'POLAND',
  SINGAPORE = 'SINGAPORE',
  SOUTH_KOREA = 'SOUTH_KOREA',
  SPAIN = 'SPAIN',
  SWEDEN = 'SWEDEN',
  SWITZERLAND = 'SWITZERLAND',
  UK = 'UK',
  USA = 'USA',
}

export enum ApplicationStatus {
  IN_REVIEW = 'IN_REVIEW',
  APPROVED = 'APPROVED',
  REJECTED = 'REJECTED',
}

export enum ObjectStatus {
  CURRENT = 'CURRENT',
  DELETED = 'DELETED',
}

@Schema({
  timestamps: true,
})
export class IndustryChangeApplication {
  @Prop()
  _id: string;

  @Prop({ required: true })
  residentSub: string;

  @Prop({ required: true, type: Current })
  current: Current;

  @Prop({ required: true, type: Requested })
  requested: Requested;

  @Prop({ required: true, default: ApplicationStatus.IN_REVIEW })
  status: ApplicationStatus;

  @Prop({ default: () => new Date() })
  submittedAt?: Date;

  @Prop({ type: Decision })
  decision?: Decision;

  @Prop({ default: () => new Date() })
  createdAt: Date;

  @Prop()
  createdBy?: string;

  @Prop({ required: true, default: () => new Date() })
  updatedAt: Date;

  @Prop()
  updatedBy?: string;

  @Prop({ required: true, enum: ObjectStatus })
  objectStatus: ObjectStatus;
}

export const IndustryChangeApplicationSchema = SchemaFactory.createForClass(
  IndustryChangeApplication,
);
