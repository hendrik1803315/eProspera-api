import { Prop, Schema } from '@nestjs/mongoose';
import {
  Industry,
  RegulatoryElection,
} from './industry-change-application.schema';

@Schema()
export class Requested {
  @Prop({ required: true })
  willWorkInPhysicalJurisdiction: boolean;

  @Prop()
  industry?: Industry;

  @Prop()
  regulatoryElection?: RegulatoryElection;

  @Prop()
  regulatoryElectionSub?: string;
}
