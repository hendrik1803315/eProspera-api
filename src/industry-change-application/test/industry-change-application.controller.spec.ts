import { Test } from '@nestjs/testing';
import { IndustryChangeApplicationController } from '../industry-change-application.controller';
import {
  IcaResponse,
  IndustryChangeApplicationService,
} from '../industry-change-application.service';
import { UsersService } from '../../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { industryChangeApplicationStub } from './stubs/industry-change-application.stub';
import { ResidentService } from '../../resident/resident.service';
import { CreateIndustryChangeApplicationDto } from '../dto/create-industry-change-application.dto';
import {
  Industry,
  RegulatoryElection,
} from '../schemas/industry-change-application.schema';
import { Requested } from '../schemas/requested.schema';
import { RejectIndustryChangeApplicationDto } from '../dto/reject-industry-change-application.dto';

jest.mock('../../resident/resident.service');

jest.mock('../industry-change-application.service', () => ({
  IndustryChangeApplicationService: jest.fn(() => ({
    find: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
    create: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
    findById: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
    delete: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
    approve: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
    reject: jest.fn().mockResolvedValue(industryChangeApplicationStub()),
  })),
}));

const jwtServiceMock = {
  verify: jest.fn(() => ({
    userId: 'mockId',
  })),
};

const usersServiceMock = {
  findById: jest.fn(() => ({
    userId: 'mockId',
  })),
};

describe('IndustryChangeApplicationController', () => {
  let controller: IndustryChangeApplicationController;
  let service: IndustryChangeApplicationService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [IndustryChangeApplicationController],
      providers: [
        IndustryChangeApplicationService,
        ResidentService,
        {
          provide: JwtService,
          useValue: jwtServiceMock,
        },
        {
          provide: UsersService,
          useValue: usersServiceMock,
        },
      ],
    }).compile();

    controller = moduleRef.get<IndustryChangeApplicationController>(
      IndustryChangeApplicationController,
    );
    service = moduleRef.get<IndustryChangeApplicationService>(
      IndustryChangeApplicationService,
    );
    jest.clearAllMocks();
  });

  describe('getIndustryChangeApplications', () => {
    describe('When getIndustryChangeApplications is called.', () => {
      let ica: IcaResponse;

      beforeEach(async () => {
        ica = await controller.getIndustryChangeApplications(
          industryChangeApplicationStub().status.toString(),
          industryChangeApplicationStub().residentSub,
        );
      });

      test('Then it should call industryChangeApplicationService.', () => {
        expect(service.find).toHaveBeenCalledWith(
          industryChangeApplicationStub().status.toString(),
          industryChangeApplicationStub().residentSub,
        );
      });

      test('Then it should return IndustryChangeApplication.', () => {
        expect(ica).toEqual(industryChangeApplicationStub());
      });
    });
  });

  describe('createIndustryChangeApplication', () => {
    describe('When createIndustryApplication is called.', () => {
      let ica: IcaResponse;
      let createIndustryChangeApplicationDto: CreateIndustryChangeApplicationDto;
      let token: string;

      beforeEach(async () => {
        const requested: Requested = {
          willWorkInPhysicalJurisdiction: false,
          industry: Industry.ENERGY,
          regulatoryElection: RegulatoryElection.AUSTRALIA,
          regulatoryElectionSub: 'Australia',
        };

        createIndustryChangeApplicationDto = {
          residentSub: '123e4567-e89b-12d3-a456-426614174000',
          requested: requested,
        };
        token = 'mocked-auth-token';
        ica = await controller.createIndustryChangeApplication(
          createIndustryChangeApplicationDto,
          token,
        );
      });

      test('Then it should call IndustryChangeApplicationService.', () => {
        expect(service.create).toHaveBeenCalledWith(
          createIndustryChangeApplicationDto,
          token,
        );
      });

      test('Then it should return IndustryChangeApplication.', () => {
        expect(ica).toEqual(industryChangeApplicationStub());
      });
    });
  });

  describe('getIndustryChangeApplicationsById', () => {
    describe('When getIndustryChangeApplicationsById is called.', () => {
      let ica: IcaResponse;

      beforeEach(async () => {
        ica = await controller.getIndustryChangeApplicationsById(
          industryChangeApplicationStub()._id,
        );
      });

      test('Then it should call IndustryChangeApplicationService.', () => {
        expect(service.findById).toHaveBeenCalledWith(
          industryChangeApplicationStub()._id,
        );
      });

      test('Then it should return IndustryChangeApplication.', () => {
        expect(ica).toEqual(industryChangeApplicationStub());
      });
    });
  });

  describe('deleteIndustryChangeApplication', () => {
    describe('When deleteIndustryChangeApplication is called.', () => {
      let ica: IcaResponse;
      let token: string;

      beforeEach(async () => {
        token = 'mocked-auth-token';
        ica = await controller.deleteIndustryChangeApplication(
          industryChangeApplicationStub()._id,
          token,
        );
      });

      test('Then it should call IndustryChangeApplicationService.', () => {
        expect(service.delete).toHaveBeenCalledWith(
          industryChangeApplicationStub()._id,
          token,
        );
      });

      test('Then it should return IndustryChangeApplication.', () => {
        expect(ica).toEqual(industryChangeApplicationStub());
      });
    });
  });

  describe('approveIndustryChangeApplication', () => {
    describe('When approveIndustryChangeApplication is called.', () => {
      let ica: IcaResponse;
      let token: string;

      beforeEach(async () => {
        token = 'mocked-auth-token';
        ica = await controller.approveIndustryChangeApplication(
          industryChangeApplicationStub()._id,
          token,
        );
      });

      test('Then it should call IndustryChangeApplicationService.', () => {
        expect(service.approve).toHaveBeenCalledWith(
          industryChangeApplicationStub()._id,
          token,
        );
      });

      test('Then it should return IndustryChangeApplication.', () => {
        expect(ica).toEqual(industryChangeApplicationStub());
      });
    });
  });

  describe('rejectIndustryChangeApplication', () => {
    describe('When rejectIndustryChangeApplication is called.', () => {
      let ica: IcaResponse;
      let token: string;
      let rejectIndustryChangeApplicationDto: RejectIndustryChangeApplicationDto;

      beforeEach(async () => {
        rejectIndustryChangeApplicationDto = {
          rejectionReason: 'Cannot work on site.',
        };
        token = 'mocked-auth-token';
        ica = await controller.rejectIndustryChangeApplication(
          industryChangeApplicationStub()._id,
          token,
          rejectIndustryChangeApplicationDto,
        );
      });

      test('Then it should call IndustryChangeApplicationService.', () => {
        expect(service.reject).toHaveBeenCalledWith(
          industryChangeApplicationStub()._id,
          token,
          rejectIndustryChangeApplicationDto,
        );
      });

      test('Then it should return IndustryChangeApplication.', () => {
        expect(ica).toEqual(industryChangeApplicationStub());
      });
    });
  });
});
