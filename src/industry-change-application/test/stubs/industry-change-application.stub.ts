import { Current } from '../../schemas/current.schema';
import { Decision } from '../../schemas/decision.schema';
import {
  ApplicationStatus,
  Industry,
  IndustryChangeApplication,
  ObjectStatus,
  RegulatoryElection,
} from '../../schemas/industry-change-application.schema';
import { Requested } from '../../schemas/requested.schema';

export const industryChangeApplicationStub = (): IndustryChangeApplication => {
  const current: Current = {
    willWorkInPhysicalJurisdiction: false,
    industry: Industry.AGRICULTURAL,
    regulatoryElection: RegulatoryElection.AUSTRALIA,
    regulatoryElectionSub: 'Australia',
  };

  const requested: Requested = {
    willWorkInPhysicalJurisdiction: false,
    industry: Industry.ENERGY,
    regulatoryElection: RegulatoryElection.AUSTRALIA,
    regulatoryElectionSub: 'Australia',
  };

  const decision: Decision = {
    decidedAt: null,
    decidedBy: null,
  };

  return {
    _id: '661442382c9aece973aae706',
    residentSub: '123e4567-e89b-12d3-a456-426614174000',
    current: current,
    requested: requested,
    status: ApplicationStatus.APPROVED,
    submittedAt: new Date('2024-04-10T13:58:24.114Z'),
    decision: decision,
    createdAt: new Date('2024-04-10T13:58:24.114Z'),
    createdBy: '661441b62c9aece973aae700',
    updatedAt: new Date('2024-04-10T13:58:24.114Z'),
    updatedBy: '661441b62c9aece973aae700',
    objectStatus: ObjectStatus.CURRENT,
  };
};
