import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { GlobalValidationPipe } from './global-validation.pipe';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(GlobalValidationPipe);

  app.enableCors();

  const DisableTryItOutPlugin = function () {
    return {
      statePlugins: {
        spec: {
          wrapSelectors: {
            allowTryItOutFor: () => () => false,
          },
        },
      },
    };
  };

  const config = new DocumentBuilder()
    .setTitle('eProspera API')
    .setDescription(
      'API for eProspera. Add Header: Authorization: Token {token}',
    )
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);

  const options = {
    swaggerOptions: {
      plugins: [DisableTryItOutPlugin],
    },
  };

  SwaggerModule.setup('api', app, document, options);

  await app.listen(3000);
}
bootstrap();
