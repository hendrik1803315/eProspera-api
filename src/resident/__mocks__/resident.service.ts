import { residentStub } from '../tests/stubs/resident.stub';

export const ResidentService = jest.fn().mockReturnValue({
  findAll: jest.fn().mockResolvedValue(residentStub()),
  create: jest.fn().mockResolvedValue(residentStub()),
  findById: jest.fn().mockResolvedValue(residentStub()),
  updateById: jest.fn().mockResolvedValue(residentStub()),
  deleteById: jest.fn().mockResolvedValue(residentStub()),
  generateResidentId: jest.fn().mockResolvedValue(residentStub()),
});
