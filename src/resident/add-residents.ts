import axios from 'axios';
import {
  RegistrationType,
  RegistrationTypeSub,
  Status,
} from './schemas/resident.schema';
import { CreateResidentDto } from './dto/create-resident.dto';
import {
  Industry,
  RegulatoryElection,
} from '../industry-change-application/schemas/industry-change-application.schema';

async function addResidents() {
  try {
    const residentsToAdd: CreateResidentDto[] = [
      {
        firstName: 'Joonas',
        lastName: 'Tamm',
        fullName: 'Joonas Tamm',
        permitNumber: 625356723,
        permitNumberQrCode: '',
        dateOfBirth: new Date('1990-01-01'),
        countryOfBirth: 'Estonia',
        email: 'joonas.tamm@example.com',
        citizenship: 'Estonia',
        gender: 'Male',
        address: {
          country: 'Estonia',
          city: 'Tallinn',
          streetAddress: 'Ravi 6',
          zipCode: '10134',
          isVerifiedAddress: true,
        },
        phoneNumber: '+3725724022',
        typeOfRegistration: RegistrationType.RESIDENCY,
        typeOfRegistrationSub: RegistrationTypeSub.HONDURAN,
        industry: Industry.MANUFACTURING,
        willWorkInPhysicalJurisdiction: true,
        regulatoryElection: RegulatoryElection.ESTONIA,
        regulatoryElectionSub: '',
        firstRegistrationDate: new Date(),
        nextSubscriptionPaymentDate: new Date('2024-07-07'),
        profilePicture: 'https://www.w3schools.com/html/img_girl.jpg',
        status: Status.ACTIVE,
      },
      {
        firstName: 'Katri',
        lastName: 'Meri',
        fullName: 'Katri Meri',
        permitNumber: 225576723,
        permitNumberQrCode: '',
        dateOfBirth: new Date('1998-02-09'),
        countryOfBirth: 'Finland',
        email: 'katri.meri@example.com',
        citizenship: 'Finland',
        gender: 'Female',
        address: {
          country: 'Finland',
          city: 'Helsinki',
          streetAddress: 'Aleksis Kiven katu 56',
          zipCode: '00510',
          isVerifiedAddress: true,
        },
        phoneNumber: '+3585724022',
        typeOfRegistration: RegistrationType.E_RESIDENCY,
        typeOfRegistrationSub: RegistrationTypeSub.INTERNATIONAL,
        industry: Industry.FOOD,
        willWorkInPhysicalJurisdiction: true,
        regulatoryElection: RegulatoryElection.FINLAND,
        regulatoryElectionSub: '',
        firstRegistrationDate: new Date(),
        nextSubscriptionPaymentDate: new Date('2024-07-07'),
        profilePicture: 'https://www.w3schools.com/html/img_girl.jpg',
        status: Status.ACTIVE,
        residencyEndDate: new Date('2032-07-05'),
      },
      {
        firstName: 'Carl',
        lastName: 'Smith',
        fullName: 'Carl Smith',
        permitNumber: 257576246,
        permitNumberQrCode: '',
        dateOfBirth: new Date('1995-02-09'),
        countryOfBirth: 'USA',
        email: 'carl.smith@example.com',
        citizenship: 'USA',
        gender: 'Male',
        address: {
          country: 'USA',
          city: 'New York',
          state: 'New York',
          streetAddress: '313 Morris Ave',
          zipCode: '07103',
          isVerifiedAddress: true,
        },
        phoneNumber: '2125724022',
        typeOfRegistration: RegistrationType.E_RESIDENCY,
        typeOfRegistrationSub: RegistrationTypeSub.INTERNATIONAL,
        industry: '',
        willWorkInPhysicalJurisdiction: false,
        regulatoryElection: '',
        regulatoryElectionSub: '',
        firstRegistrationDate: new Date(),
        nextSubscriptionPaymentDate: new Date('2024-02-06'),
        profilePicture: 'https://www.w3schools.com/html/img_girl.jpg',
        status: Status.ACTIVE,
        residencyEndDate: new Date('2030-02-01'),
      },
    ];

    for (const resident of residentsToAdd) {
      await axios.post('http://localhost:3000/residents/new', resident);
      console.log(`Added resident: ${resident.fullName}`);
    }

    console.log('All residents added successfully.');
  } catch (error) {
    console.error('Error adding residents:', error);
  }
}

addResidents();
