import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsEmail,
  IsBoolean,
  IsEnum,
  ValidateNested,
  IsDefined,
  IsOptional,
  IsDateString,
} from 'class-validator';
import { Type } from 'class-transformer';
import {
  RegistrationType,
  RegistrationTypeSub,
  Status,
} from '../schemas/resident.schema';
import { ApiProperty } from '@nestjs/swagger';

export class AddressDto {
  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Country',
    example: 'USA',
  })
  country: string;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'City',
    example: 'New York',
  })
  city: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'State',
    example: 'New York',
  })
  state?: string;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Street address',
    example: '313 Morris Ave',
  })
  streetAddress: string;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Zipcode',
    example: '07103',
  })
  zipCode: string;

  @IsBoolean()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Is verified address',
    example: true,
  })
  isVerifiedAddress: boolean;
}

export class CreateResidentDto {
  @ApiProperty({
    description: 'First name',
    example: 'Thomas',
  })
  @IsString()
  @IsNotEmpty()
  @IsDefined()
  firstName: string;

  @ApiProperty({
    description: 'Last name',
    example: 'Anderson',
  })
  @IsString()
  @IsNotEmpty()
  @IsDefined()
  lastName: string;

  @ApiProperty({
    description: 'Full name',
    example: 'Thomas Anderson',
  })
  @IsString()
  @IsNotEmpty()
  @IsDefined()
  fullName: string;

  @ApiProperty({
    description: 'Permit number',
    example: 5253453,
  })
  @IsNumber()
  @IsNotEmpty()
  @IsDefined()
  permitNumber: number;

  @ApiProperty({
    description: 'Permit number qr code',
    example: 67435321,
  })
  @IsString()
  permitNumberQrCode?: string;

  @ApiProperty({
    description: 'Date of birth',
    example: '1990-01-01',
  })
  @IsDateString()
  @IsNotEmpty()
  @IsDefined()
  dateOfBirth: Date;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Country of birth',
    example: 'USA',
  })
  countryOfBirth: string;

  @IsEmail()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Email',
    example: 'thomas.anderson@sample.com',
  })
  email: string;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Citizenship',
    example: 'USA',
  })
  citizenship: string;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Gender',
    example: 'Male',
  })
  gender: string;

  @ValidateNested()
  @Type(() => AddressDto)
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Aadress',
    example: {
      country: 'USA',
      city: 'New York',
      streetAddress: '313 Morris Ave',
      zipCode: '07103',
      isVerifiedAddress: true,
    },
  })
  address: AddressDto;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Phone number',
    example: '2125724022',
  })
  phoneNumber: string;

  @IsEnum(RegistrationType)
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Type of registration',
    example: 'LIMITED_E_RESIDENCY',
  })
  typeOfRegistration: RegistrationType;

  @IsOptional()
  @ApiProperty({
    description: 'Type of registration sub',
    example: 'INTERNATIONAL',
  })
  typeOfRegistrationSub?: RegistrationTypeSub;

  @IsOptional()
  @ApiProperty({
    description: 'Industry',
    example: 'WASTE_MANAGEMENT',
  })
  industry?: string;

  @IsBoolean()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Will work in physical juristicion',
    example: false,
  })
  willWorkInPhysicalJurisdiction: boolean;

  @IsOptional()
  @ApiProperty({
    description: 'Regulatory Election',
    example: 'USA',
  })
  regulatoryElection?: string;

  @IsOptional()
  @ApiProperty({
    description: 'Regulatory election sub',
    example: '',
  })
  regulatoryElectionSub?: string;

  @IsDateString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'First registration date',
    example: '2024-03-03',
  })
  firstRegistrationDate: Date;

  @IsDateString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Next subscription payment date',
    example: '2024-06-06',
  })
  nextSubscriptionPaymentDate: Date;

  @IsString()
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Profile picture',
    example: 'https://www.w3schools.com/html/img_girl.jpg',
  })
  profilePicture: string;

  @IsEnum(Status)
  @IsNotEmpty()
  @IsDefined()
  @ApiProperty({
    description: 'Status',
    example: 'ACTIVE',
  })
  status: Status;

  @IsOptional()
  @ApiProperty({
    description: 'Residency end date',
    example: '2025-06-01',
  })
  residencyEndDate?: Date;
}
