import {
  IsString,
  IsNumber,
  IsEmail,
  IsBoolean,
  IsEnum,
  ValidateNested,
  IsOptional,
  IsDateString,
} from 'class-validator';
import { Type } from 'class-transformer';
import {
  RegistrationType,
  RegistrationTypeSub,
  Status,
} from '../schemas/resident.schema';
import { ApiProperty } from '@nestjs/swagger';

export class AddressDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Country',
    example: 'USA',
  })
  country?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'City',
    example: 'New York',
  })
  city?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'State',
    example: 'New York',
  })
  state?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Street address',
    example: '313 Morris Ave',
  })
  streetAddress?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Zipcode',
    example: '07103',
  })
  zipCode?: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Is verified address',
    example: true,
  })
  isVerifiedAddress?: boolean;
}

export class UpdateResidentDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'First name',
    example: 'Thomas',
  })
  firstName?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Last name',
    example: 'Anderson',
  })
  lastName?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Full name',
    example: 'Thomas Anderson',
  })
  fullName?: string;

  @IsNumber()
  @IsOptional()
  @ApiProperty({
    description: 'Permit number',
    example: 5253453,
  })
  permitNumber?: number;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Permit number qr code',
    example: 67435321,
  })
  permitNumberQrCode?: string;

  @IsDateString()
  @IsOptional()
  @ApiProperty({
    description: 'Date of birth',
    example: '1990-01-01',
  })
  dateOfBirth?: Date;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Country of birth',
    example: 'USA',
  })
  countryOfBirth?: string;

  @IsEmail()
  @IsOptional()
  @ApiProperty({
    description: 'Email',
    example: 'thomas.anderson@sample.com',
  })
  email?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Citizenship',
    example: 'USA',
  })
  citizenship?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Gender',
    example: 'Male',
  })
  gender?: string;

  @ValidateNested()
  @Type(() => AddressDto)
  @IsOptional()
  @ApiProperty({
    description: 'Aadress',
    example: {
      country: 'USA',
      city: 'New York',
      streetAddress: '313 Morris Ave',
      zipCode: '07103',
      isVerifiedAddress: true,
    },
  })
  address?: AddressDto;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Phone number',
    example: '2125724022',
  })
  phoneNumber?: string;

  @IsEnum(RegistrationType)
  @IsOptional()
  @ApiProperty({
    description: 'Type of registration',
    example: 'LIMITED_E_RESIDENCY',
  })
  typeOfRegistration?: RegistrationType;

  @ApiProperty({
    description: 'Type of registration sub',
    example: 'INTERNATIONAL',
  })
  @IsOptional()
  @IsEnum(RegistrationTypeSub)
  typeOfRegistrationSub?: RegistrationTypeSub;

  @ApiProperty({
    description: 'Industry',
    example: 'WASTE_MANAGEMENT',
  })
  @IsOptional()
  industry?: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Will work in physical juristicion',
    example: false,
  })
  willWorkInPhysicalJurisdiction?: boolean;

  @ApiProperty({
    description: 'Regulatory Election',
    example: 'USA',
  })
  @IsOptional()
  regulatoryElection?: string;

  @ApiProperty({
    description: 'Regulatory election sub',
    example: '',
  })
  @IsOptional()
  regulatoryElectionSub?: string;

  @IsDateString()
  @ApiProperty({
    description: 'First registration date',
    example: '2024-03-03',
  })
  @IsOptional()
  firstRegistrationDate?: Date;

  @IsDateString()
  @ApiProperty({
    description: 'Next subscription payment date',
    example: '2024-06-06',
  })
  @IsOptional()
  nextSubscriptionPaymentDate?: Date;

  @IsString()
  @ApiProperty({
    description: 'Profile picture',
    example: 'https://www.w3schools.com/html/img_girl.jpg',
  })
  @IsOptional()
  profilePicture?: string;

  @IsEnum(Status)
  @ApiProperty({
    description: 'Status',
    example: 'ACTIVE',
  })
  @IsOptional()
  status?: Status;

  @ApiProperty({
    description: 'Residency end date',
    example: '2025-06-01',
  })
  @IsOptional()
  @IsDateString()
  residencyEndDate?: Date;
}
