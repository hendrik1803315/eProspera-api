import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ResidentService } from './resident.service';
import { Resident } from './schemas/resident.schema';
import { UserGuard } from '../users/users.guard';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateResidentDto } from './dto/create-resident.dto';
import { UpdateResidentDto } from './dto/update-resident.dto';

@ApiTags('Residents')
@Controller('residents')
export class ResidentController {
  constructor(private residentService: ResidentService) {}

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Got all residents.',
  })
  @ApiBadRequestResponse({
    description: 'Could not find residents.',
  })
  @Get()
  async getAllResidents(): Promise<Resident[]> {
    return this.residentService.findAll();
  }

  @ApiCreatedResponse({
    description: 'Created a Resident.',
  })
  @ApiBadRequestResponse({
    description: 'Could not create a resident.',
  })
  @Post('new')
  async createResident(
    @Body()
    residents: CreateResidentDto,
  ): Promise<Resident> {
    return this.residentService.create(residents);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Found a resident.',
  })
  @ApiBadRequestResponse({
    description: 'Could not find a resident.',
  })
  @Get(':subId')
  async getResident(
    @Param('subId')
    subId: string,
  ): Promise<Resident> {
    return this.residentService.findById(subId);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Updated a resident.',
  })
  @ApiBadRequestResponse({
    description: 'Could not update a resident.',
  })
  @Put(':subId')
  async updateResident(
    @Param('subId')
    subId: string,
    @Body()
    resident: UpdateResidentDto,
  ): Promise<Resident> {
    return this.residentService.updateById(subId, resident);
  }

  @UseGuards(UserGuard)
  @ApiCreatedResponse({
    description: 'Deleted a resident.',
  })
  @ApiBadRequestResponse({
    description: 'Could not delete a resident',
  })
  @Delete(':subId')
  async deleteResident(
    @Param('subId')
    subId: string,
  ): Promise<Resident> {
    return this.residentService.deleteById(subId);
  }
}
