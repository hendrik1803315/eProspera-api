import { Module } from '@nestjs/common';
import { ResidentController } from './resident.controller';
import { ResidentService } from './resident.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ResidentSchema } from './schemas/resident.schema';
import { UsersService } from 'src/users/users.service';
import { UserEntitySchema } from 'src/users/schemas/users.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Resident', schema: ResidentSchema },
      { name: 'Users', schema: UserEntitySchema },
    ]),
  ],
  controllers: [ResidentController],
  providers: [ResidentService, UsersService],
})
export class ResidentModule {}
