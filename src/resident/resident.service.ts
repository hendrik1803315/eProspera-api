import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Resident } from './schemas/resident.schema';
import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { CreateResidentDto } from './dto/create-resident.dto';
import { UpdateResidentDto } from './dto/update-resident.dto';

@Injectable()
export class ResidentService {
  constructor(
    @InjectModel(Resident.name)
    private residentModel: mongoose.Model<Resident>,
  ) {}

  async findAll(): Promise<Resident[]> {
    const residents = await this.residentModel.find();
    return residents;
  }

  async create(resident: CreateResidentDto): Promise<Resident> {
    const existingResident = await this.residentModel.findOne({
      email: resident.email,
    });
    if (existingResident) {
      throw new ConflictException('A resident with this email already exists');
    }

    const residentWithIds = {
      subID: uuidv4(),
      ...resident,
    };

    if (typeof resident.dateOfBirth === 'string') {
      resident.dateOfBirth = new Date(resident.dateOfBirth);
    }

    const res = await this.residentModel.create(residentWithIds);
    return res;
  }

  async findById(subID: string): Promise<Resident> {
    try {
      const resident = await this.residentModel.findOne({ subID: subID });
      if (!resident) {
        throw new NotFoundException('Resident not found');
      }
      return resident;
    } catch (error) {
      if (error.name === 'CastError' && error.kind === 'ObjectId') {
        throw new BadRequestException('Invalid Resident ID');
      } else {
        throw error;
      }
    }
  }

  async updateById(
    subId: string,
    updateResident: UpdateResidentDto,
  ): Promise<Resident> {
    const resident = await this.residentModel.findOne({ subID: subId });

    if (!resident) {
      throw new Error('Resident not found');
    }

    Object.assign(resident, updateResident);

    return resident.save();
  }

  async deleteById(subId: string): Promise<Resident> {
    const resident = await this.residentModel.findOne({ subID: subId });

    if (!resident) {
      throw new Error('Resident not found');
    }

    return await this.residentModel.findByIdAndDelete(resident._id);
  }

  generateResidentId(region: string): string {
    const uuid = uuidv4().toLowerCase().replace(/-/g, '');
    return `${region}:${uuid.substr(0, 8)}-${uuid.substr(8, 4)}-${uuid.substr(12, 4)}-${uuid.substr(16, 4)}-${uuid.substr(20)}`;
  }
}
