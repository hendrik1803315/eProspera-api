import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export enum RegistrationType {
  E_RESIDENCY = 'E_RESIDENCY',
  RESIDENCY = 'RESIDENCY',
  LIMITED_E_RESIDENCT = 'LIMITED_E_RESIDENCY',
}

export enum RegistrationTypeSub {
  HONDURAN = 'HONDURAN',
  INTERNATIONAL = 'INTERNATIONAL',
}

export enum Status {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

@Schema()
export class Address {
  @Prop({ required: true })
  country: string;

  @Prop({ required: true })
  city: string;

  @Prop()
  state?: string;

  @Prop({ required: true })
  streetAddress: string;

  @Prop({ required: true })
  zipCode: string;

  @Prop({ required: true })
  isVerifiedAddress: boolean;
}

@Schema({
  timestamps: true,
})
export class Resident {
  @Prop({ required: true, unique: true }) // Same as Amazon Cognito identity pool id
  subID: string;

  @Prop({ required: true })
  firstName: string;

  @Prop({ required: true })
  lastName: string;

  @Prop({ required: true })
  fullName: string;

  @Prop({ required: true })
  permitNumber: number;

  @Prop()
  permitNumberQrCode?: string;

  @Prop({ required: true })
  dateOfBirth: Date;

  @Prop({ required: true })
  countryOfBirth: string;

  @Prop({ required: true })
  email: string;

  @Prop({ required: true })
  citizenship: string;

  @Prop({ required: true })
  gender: string;

  @Prop({ required: true, type: Address }) // Specify the type here
  address: Address;

  @Prop({ required: true })
  phoneNumber: string;

  @Prop({ required: true })
  typeOfRegistration: RegistrationType;

  @Prop()
  typeOfRegistrationSub?: RegistrationTypeSub;

  @Prop()
  industry?: string;

  @Prop({ required: true })
  willWorkInPhysicalJurisdiction: boolean;

  @Prop()
  regulatoryElection?: string;

  @Prop()
  regulatoryElectionSub?: string;

  @Prop({ required: true })
  firstRegistrationDate: Date;

  @Prop({ required: true })
  nextSubscriptionPaymentDate: Date;

  @Prop({ required: true })
  profilePicture: string;

  @Prop({ required: true })
  status: Status;

  @Prop()
  residencyEndDate?: Date;
}

export const AddressSchema = SchemaFactory.createForClass(Address);
export const ResidentSchema = SchemaFactory.createForClass(Resident);
