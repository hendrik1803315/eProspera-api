import { Test } from '@nestjs/testing';
import { ResidentController } from '../resident.controller';
import { ResidentService } from '../resident.service';
import {
  Address,
  RegistrationType,
  Resident,
  Status,
} from '../schemas/resident.schema';
import { CreateResidentDto } from '../dto/create-resident.dto';
import { residentStub } from './stubs/resident.stub';
import { UpdateResidentDto } from '../dto/update-resident.dto';
import { UsersService } from '../../users/users.service';
import { JwtService } from '@nestjs/jwt';

jest.mock('../resident.service');

const jwtServiceMock = {
  verify: jest.fn(() => ({
    userId: 'mockUserId',
  })),
};

const usersServiceMock = {
  findById: jest.fn(() => ({
    userId: 'mockUserId',
  })),
};

describe('ResidentController', () => {
  let controller: ResidentController;
  let service: ResidentService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      controllers: [ResidentController],
      providers: [
        ResidentService,
        {
          provide: JwtService,
          useValue: jwtServiceMock,
        },
        {
          provide: UsersService,
          useValue: usersServiceMock,
        },
      ],
    }).compile();

    controller = moduleRef.get<ResidentController>(ResidentController);
    service = moduleRef.get<ResidentService>(ResidentService);
    jest.clearAllMocks();
  });

  describe('createResident', () => {
    describe('When createResident is called.', () => {
      let resident: Resident;
      let createResidentDto: CreateResidentDto;
      beforeEach(async () => {
        const address: Address = {
          country: 'Estonia',
          city: 'Tallinn',
          streetAddress: 'Ravi 6',
          zipCode: '101134',
          isVerifiedAddress: true,
        };

        createResidentDto = {
          firstName: 'Stub',
          lastName: 'Saak',
          fullName: 'Stub Saak',
          permitNumber: 123456789,
          dateOfBirth: new Date('2024-04-10T13:58:24.114Z'),
          countryOfBirth: 'Estonia',
          email: 'stub@example.com',
          citizenship: 'Estonia',
          gender: 'Male',
          address: address,
          phoneNumber: '+372 55555555',
          typeOfRegistration: RegistrationType.RESIDENCY,
          willWorkInPhysicalJurisdiction: true,
          firstRegistrationDate: new Date('2024-04-10T13:58:24.114Z'),
          nextSubscriptionPaymentDate: new Date('2024-04-10T13:58:24.114Z'),
          profilePicture: 'profile.jpg',
          status: Status.ACTIVE,
        };
        resident = await controller.createResident(createResidentDto);
      });

      test('Then it should call residentService.', () => {
        expect(service.create).toHaveBeenCalledWith(createResidentDto);
      });

      test('Then it should return a resident.', () => {
        expect(resident).toEqual(residentStub());
      });
    });
  });

  describe('getResident', () => {
    describe('When GetResident is called', () => {
      let resident: Resident;

      beforeEach(async () => {
        resident = await controller.getResident(residentStub().subID);
      });

      test('Then it should call residentService.', () => {
        expect(service.findById).toHaveBeenCalledWith(residentStub().subID);
      });

      test('Then it should return a resident.', () => {
        expect(resident).toEqual(residentStub());
      });
    });
  });

  describe('getAllResidents', () => {
    describe('When getAllResidents is called', () => {
      let residents: Resident[];

      beforeEach(async () => {
        residents = await controller.getAllResidents();
      });

      test('Then it should call residentService.', () => {
        expect(service.findAll).toHaveBeenCalled();
      });

      test('Then it should return residents.', () => {
        expect(residents).toEqual(residentStub());
      });
    });
  });

  describe('updateResident', () => {
    describe('When getAllResidents is called', () => {
      let resident: Resident;
      let updateResidentDto: UpdateResidentDto;

      beforeEach(async () => {
        updateResidentDto = {
          firstName: 'Karl',
        };
        resident = await controller.updateResident(
          residentStub().subID,
          updateResidentDto,
        );
      });

      test('Then it should call residentService.', () => {
        expect(service.updateById).toHaveBeenCalledWith(
          residentStub().subID,
          updateResidentDto,
        );
      });

      test('Then it should return a resident.', () => {
        expect(resident).toEqual(residentStub());
      });
    });
  });

  describe('deleteResident', () => {
    describe('When deleteResident is called', () => {
      let resident: Resident;

      beforeEach(async () => {
        resident = await controller.deleteResident(residentStub().subID);
      });

      test('Then it should call residentService.', () => {
        expect(service.deleteById).toHaveBeenCalledWith(residentStub().subID);
      });

      test('Then it should return a resident.', () => {
        expect(resident).toEqual(residentStub());
      });
    });
  });
});
