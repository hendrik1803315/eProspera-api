import {
  Address,
  RegistrationType,
  Resident,
  Status,
} from '../../schemas/resident.schema';

export const residentStub = (): Resident => {
  const address: Address = {
    country: 'Estonia',
    city: 'Tallinn',
    streetAddress: 'Ravi 6',
    zipCode: '101134',
    isVerifiedAddress: true,
  };

  return {
    subID: '123e4567-e89b-12d3-a456-426614174002',
    firstName: 'Stub',
    lastName: 'Saak',
    fullName: 'Stub Saak',
    permitNumber: 123456789, // Example permitNumber
    dateOfBirth: new Date('2024-04-10T13:58:24.114Z'),
    countryOfBirth: 'Estonia',
    email: 'stub@example.com',
    citizenship: 'Estonia',
    gender: 'Male',
    address: address,
    phoneNumber: '+372 55555555',
    typeOfRegistration: RegistrationType.RESIDENCY,
    willWorkInPhysicalJurisdiction: true,
    firstRegistrationDate: new Date('2024-04-10T13:58:24.114Z'),
    nextSubscriptionPaymentDate: new Date('2024-04-10T13:58:24.114Z'),
    profilePicture: 'profile.jpg',
    status: Status.ACTIVE,
  };
};
