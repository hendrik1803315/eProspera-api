import { userStub } from '../tests/stubs/users.stub';

export const UsersService = jest.fn().mockReturnValue({
  createUser: jest.fn().mockResolvedValue(userStub()),
  loginUser: jest.fn().mockResolvedValue(userStub()),
  findById: jest.fn().mockResolvedValue(userStub()),
  getCurrentUser: jest.fn().mockResolvedValue(userStub()),
  buildUserResponse: jest.fn().mockResolvedValue(userStub()),
});
