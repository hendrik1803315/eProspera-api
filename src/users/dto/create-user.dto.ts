import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Matches, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    description: 'The email',
    example: 'sample@gmail.com',
  })
  email: string;

  @IsNotEmpty()
  @MinLength(6, { message: 'Username must be at least 6 characters long' })
  @ApiProperty({
    description: 'The username',
    example: 'sample123',
  })
  username: string;

  @IsNotEmpty()
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]+$/, {
    message:
      'Password must contain at least one uppercase letter, one lowercase letter, and one digit',
  })
  @ApiProperty({
    description: 'The password',
    example: 'Sample123',
  })
  password: string;
}
