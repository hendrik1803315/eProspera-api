import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { Users } from '../schemas/users.schema';
import { UsersService } from '../users.service';
import { jwtConstants } from '../users.constants';
import { JwtService } from '@nestjs/jwt';

export interface ExpressRequest extends Request {
  user?: Users;
}

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}
  async use(req: ExpressRequest, res: Response, next: NextFunction) {
    if (!req.headers['authorization']) {
      req.user = null;
      next();
      return;
    }

    const token = req.headers['authorization'].split(' ')[1];

    try {
      const decode = this.jwtService.verify(token, {
        secret: jwtConstants.secret,
      }) as { _id: string };
      const user = await this.usersService.findById(decode._id);

      req.user = user;

      next();
    } catch (error) {
      req.user = null;
      next();
    }
  }
}
