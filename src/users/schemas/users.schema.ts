import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { hash } from 'bcrypt';
import mongoose from 'mongoose';

@Schema()
export class Users {
  @Prop({ type: mongoose.Schema.Types.ObjectId, auto: true })
  _id: string;

  @Prop()
  email: string;

  @Prop()
  username: string;

  @Prop({ select: false })
  password: string;
}

export const UserEntitySchema = SchemaFactory.createForClass(Users);

UserEntitySchema.pre<Users>('save', async function (next: () => void) {
  this.password = await hash(this.password, 10);
  next();
});
