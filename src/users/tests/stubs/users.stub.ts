import { Users } from 'src/users/schemas/users.schema';

interface UserStub extends Omit<Users, '_id'> {}

export const userStub = (): UserStub => {
  return {
    email: 'stub@gmail.com',
    username: 'Stubbie',
    password: 'Stubbie123',
  };
};

export const userStubWithId = (): Users => {
  return {
    ...userStub(),
    _id: '6614508284d71ae3b41a4a2b',
  };
};
