import { Test } from '@nestjs/testing';
import { UsersController } from '../users.controller';
import { UsersService } from '../users.service';
import { userStub, userStubWithId } from './stubs/users.stub';
import { UserResponseType } from '../types/user-response.type';
import { ExpressRequest } from '../middlewares/auth.middleware';
import { HttpException } from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { LoginDto } from '../dto/login-user.dto';

jest.mock('../users.service');

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      controllers: [UsersController],
      providers: [UsersService],
    }).compile();

    controller = moduleRef.get<UsersController>(UsersController);
    service = moduleRef.get<UsersService>(UsersService);
    jest.clearAllMocks();
  });

  describe('createUser', () => {
    describe('When createUser is called.', () => {
      let user: UserResponseType;
      const createUserDto: CreateUserDto = new CreateUserDto();
      beforeEach(async () => {
        createUserDto.email = 'stub@gmail.com';
        createUserDto.username = 'Stubbie';
        createUserDto.password = 'Stubbie123';
        user = await controller.createUser(createUserDto);
      });

      test('Then it should call usersService.', () => {
        expect(service.createUser).toHaveBeenCalledWith(createUserDto);
      });

      test('Then it should return a user.', () => {
        expect(user).toEqual(userStub());
      });
    });
  });

  describe('login', () => {
    describe('When login is called.', () => {
      let user: UserResponseType;
      const loginUserDto: LoginDto = new LoginDto();
      beforeEach(async () => {
        loginUserDto.email = 'stub@gmail.com';
        loginUserDto.password = 'Stubbie123';
        user = await controller.login(loginUserDto);
      });

      test('Then it should call usersService.', () => {
        expect(service.loginUser).toHaveBeenCalledWith(loginUserDto);
      });

      test('Then it should return a user.', () => {
        expect(user).toEqual(userStub());
      });
    });
  });

  describe('currentUser', () => {
    describe('When currentUser is called.', () => {
      let user: UserResponseType;
      beforeEach(async () => {
        const request: Partial<ExpressRequest> = { 
          user: userStubWithId(),
          headers: { authorization: 'Bearer Mock_token' }
         };
        user = await controller.currentUser(request as ExpressRequest);
      });

      test('Then it should return a user.', () => {
        expect(user).toEqual(userStub());
      });
    });
  });

  describe('When currentUser is called without a user.', () => {
    test('Then it should throw an Unauthorized error.', async () => {
      const request: Partial<ExpressRequest> = {};
      await expect(
        controller.currentUser(request as ExpressRequest),
      ).rejects.toThrow(HttpException);
    });
  });
});
