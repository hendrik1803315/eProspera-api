import { Users } from '../schemas/users.schema';

export type UserResponseType = Omit<Users, 'password'> & { token: string };
