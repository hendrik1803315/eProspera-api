import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Request,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UserResponseType } from './types/user-response.type';
import { LoginDto } from './dto/login-user.dto';
import { ExpressRequest } from './middlewares/auth.middleware';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('User')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @ApiCreatedResponse({
    description: 'Created user object',
  })
  @ApiBadRequestResponse({
    description: 'User cannot register. Try again!',
  })
  async createUser(
    @Body()
    createUserDto: CreateUserDto,
  ): Promise<UserResponseType> {
    const user = await this.usersService.createUser(createUserDto);
    return this.usersService.buildUserResponse(user);
  }

  @Post('login')
  @ApiCreatedResponse({
    description: 'User Logged in.',
  })
  @ApiBadRequestResponse({
    description: 'Cannot Log in.',
  })
  async login(@Body() loginDto: LoginDto): Promise<UserResponseType> {
    const user = await this.usersService.loginUser(loginDto);
    return this.usersService.buildUserResponse(user);
  }

  @ApiCreatedResponse({
    description: 'Current User.',
  })
  @ApiBadRequestResponse({
    description: 'Cannot find current user.',
  })
  @Get('user')
  async currentUser(
    @Request() request: ExpressRequest,
  ): Promise<UserResponseType> {
    const authorizationHeader = request.headers && request.headers['authorization'];
    if (!authorizationHeader) {
      throw new HttpException(
        'Please add Authorization header',
        HttpStatus.UNAUTHORIZED,
      );
    }
    if (!request.user) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
    return this.usersService.buildUserResponse(request.user);
  }
}
