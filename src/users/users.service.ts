import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Users } from './schemas/users.schema';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UserResponseType } from './types/user-response.type';
import { LoginDto } from './dto/login-user.dto';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from './users.constants';

@Injectable()
export class UsersService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectModel(Users.name) private userModel: Model<Users>,
  ) {}

  async createUser(createUserDto: CreateUserDto): Promise<Users> {
    const user = await this.userModel.findOne({ email: createUserDto.email });

    if (user) {
      throw new HttpException(
        'Email is already taken',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    const createdUser = new this.userModel(createUserDto);
    return createdUser.save();
  }

  async loginUser(loginDto: LoginDto): Promise<Users> {
    const user = await this.userModel
      .findOne({ email: loginDto.email })
      .select('+password');

    if (!user) {
      throw new HttpException(
        'User not found',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    const isPasswordCorrect = await compare(loginDto.password, user.password);

    if (!isPasswordCorrect) {
      throw new HttpException(
        'Incorrect password',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    return user;
  }

  buildUserResponse(userEntity: Users): UserResponseType {
    return {
      _id: userEntity._id,
      username: userEntity.username,
      email: userEntity.email,
      token: this.generateJwt(userEntity),
    };
  }

  generateJwt(userEntity: Users): string {
    return this.jwtService.sign(
      { _id: userEntity._id, email: userEntity.email },
      { expiresIn: '1h', secret: jwtConstants.secret },
    );
  }

  async findById(_id: string): Promise<Users> {
    return this.userModel.findOne({ _id });
  }

  async getCurrentUser(token: string): Promise<Users> {
    const decode = this.jwtService.verify(token, {
      secret: jwtConstants.secret,
    }) as { _id: string };

    const user = await this.userModel.findById(decode._id).exec();

    if (!user) {
      throw new HttpException(
        'User not found',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    return user;
  }
}
